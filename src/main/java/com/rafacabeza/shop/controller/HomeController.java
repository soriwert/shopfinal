package com.rafacabeza.shop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(Model model) {
		String name = "Teclado USB";
		double price = 12.5;
		String code = "KB01";
		
		model.addAttribute("name", name);
		model.addAttribute("code", code);
		model.addAttribute("price", price);

		
		return "home";
	}	
}
